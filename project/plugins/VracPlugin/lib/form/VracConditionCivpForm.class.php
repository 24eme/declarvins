<?php
class VracConditionCivpForm extends VracConditionForm 
{
    public function configure() {
        parent::configure();
        unset($this['has_transaction'], $this['reference_contrat_pluriannuel']);
        if ($this->getObject()->vendeur->famille != EtablissementFamilles::FAMILLE_NEGOCIANT && $this->getObject()->vendeur->sous_famille != EtablissementFamilles::SOUS_FAMILLE_VINIFICATEUR) {
            unset($this['premiere_mise_en_marche']);
        }
    }

    protected function doUpdateObject($values) {
        parent::doUpdateObject($values);
        if (!sfContext::getInstance()->getUser()->hasCredential(myUser::CREDENTIAL_OPERATEUR)) {
        	$this->getObject()->has_transaction = 1;
        } else {
            $this->getObject()->has_transaction = 0;
        }
    }
}